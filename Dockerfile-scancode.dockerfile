FROM python:3.6-slim-buster

ENV BUILD_VERSION=3.2.3

RUN apt-get update \
 && apt-get install -y bzip2 xz-utils zlib1g libxml2-dev libxslt1-dev libgomp1 libpopt0\
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ADD "https://github.com/nexB/scancode-toolkit/archive/v${BUILD_VERSION}.tar.gz" scancode-toolkit.tar.gz
RUN mkdir scancode-toolkit && tar -xf scancode-toolkit.tar.gz -C scancode-toolkit --strip-components 1

WORKDIR scancode-toolkit

RUN ./configure

# Run scancode once for initial configuration, with --reindex-licenses to create the base license index
RUN ./scancode --reindex-licenses

ENV PATH=$HOME/scancode-toolkit:$PATH

# Set entrypoint to be the scancode command, allows to run the generated docker image directly with the scancode arguments: `docker run (...) <containername> <scancode arguments>`
ENTRYPOINT ["./scancode"]
