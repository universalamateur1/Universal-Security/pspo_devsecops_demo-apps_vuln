# Test Webserver

The original code is from a [CyberFRAT](https://cyberfrat.com/event/devsecops/) DevSecOps training.

- [Link to github repo](https://github.com/deepshankaryadav/CyberFRAT-DevSecOps-Training-Sample-Flask-App/tree/master/vulnerable-app)


### Installation 

Move into project directory `vulnerable-flask-app`

##### Install Python version and virtualenv

```bash
> export PYTHON_VERSION=3.6
> sudo apt install python${PYTHON_VERSION}
> python${PYTHON_VERSION} -m pip install virtualenv
```

##### Create and activate virtual environment

```bash
> python${PYTHON_VERSION} -m virtualenv venv
> source venv/bin/activate
> python -m pip install --upgrade pip
```

##### Install required packages

```bash
> pip install -r requirements.txt
```

### Run locally

```bash
> cd app
> python app.py
```

With `http:\\localhost:5050` you get the following output in the browser:

You have reached the Vulnerable App.

Here are the list of urls you would need while using this application

 API | Function | Allowed methods | Description
 ----|----------|-----------------|-------------
 `/`  |  `sitemap` |  `HEAD,OPTIONS,GET` | Create overview of all interfaces
 `/login` | `login` |  `POST,OPTIONS` | User login, return value is an `access_token` needed to access the interfaces `/fetch/customer`, `/get/<customerID>` and `/search`
`/get/<customerID>` | `get_customer` | `GET` | Get customer data from customer with ID <customerID>
 `/search` | `search_customer` |  `POST,OPTIONS` | Search for customer with username `"search": "username"`
 `/fetch/customer` | `fetch_customer` | `POST,OPTIONS` | Fetch customer with ID `"id": "ID"`
 `/register/user` | `reg_customer` |  `POST,OPTIONS` | Register user with `"username": "Name"` and `"password": "passwd"`
 `/register/customer` | `reg_user` | `POST,OPTIONS` | Register customer with `"username": "username"`, `"password": "password`, `"first_name": "first_name"`, `"last_name": "last_name`, `"email": "email"` and `"ccn": "ccn"`
 `/xxe_uploader` | `hello` |  `POST,HEAD,OPTIONS,GET` | see `/xxe`
 `/yaml_hammer` | `yaml_hammer` | `POST,OPTIONS` | see `/yaml_hammer`
 `/yaml` | `yaml_upload` |  `HEAD,OPTIONS,GET` | Web UI to select a yml file and display it on a new web page converting it to json format beforehand (with `/yaml_hammer`)
 `/xxe` | `index` |  `HEAD,OPTIONS,GET` | Web UI to select a docx file to convert it to text and display it on another web page (with `/xxe_uploader`)

### Test

To execute the functional tests proceed as follows

```bash
> cd tests
> ./run_tests.sh
```


## DevSecOps

### Scanners

Name | .gitlab-ci.yml | Scanner | Description
-----|----------------|---------|------------
Code-Quality.gitlab-ci.yml  | | |
Secret-Detection | [Secret-Detection.gitlab-ci.yml](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/Secret-Detection.gitlab-ci.yml) | |
Container-Scanning | | |
Dependency-Scanning | | |
License-Scanning | | |
SAST  |  | [bandit SAST](https://gitlab.com/gitlab-org/security-products/analyzers/bandit), ESLint, Semgrep, Snyk Code, SonarQube, WhiteSource
SAST-IaC | | KICS-IaC |
