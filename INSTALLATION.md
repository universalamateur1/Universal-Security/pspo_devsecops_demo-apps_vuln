
## SonarQube

1. Download and unzip the Scanner for Linux

Visit the [official documentation of the Scanner](https://docs.sonarqube.org/latest/analysis/scan/sonarscanner/) to download the latest version, and add the `bin` directory to the `PATH` environment variable

2. Execute the Scanner from your computer
Running a SonarQube analysis is straighforward. You just need to execute the following commands in your project's folder.


sonar-scanner \
  -Dsonar.projectKey=pspo_devsecops_demo-apps_vulnerable-flask-app_AX_6pyNqBRv7irHdJKHI \
  -Dsonar.sources=. \
  -Dsonar.host.url=http://sonar.intranet.roche.com \
  -Dsonar.login=9880e8a2d40cbd059226b9e6da777fa8f65d8e7d


Please visit the [official documentation of the Scanner](http://sonar.intranet.roche.com/documentation/analysis/scan/sonarscanner/) for more details.

Once the analysis is completed, this page will automatically refresh and you will be able to browse the analysis results.