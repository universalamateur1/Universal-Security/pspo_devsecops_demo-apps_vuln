aiohttp==3.7.4.post0
async-timeout==3.0.1
attrs==21.2.0
Babel==2.9.1
banal==1.0.6
bitarray==2.5.1
blinker==1.4
boolean.py==3.8
boto3==1.18.48
botocore==1.21.48
certifi==2021.5.30
chardet==4.0.0
charset-normalizer==2.0.6
click==8.0.1
cyclonedx-bom==3.2.1
cyclonedx-python-lib==2.3.0
dataclasses==0.6
DateTime==4.4
docutils==0.17.1
exception==0.1.0
extractcode-7z==16.5.210531
extractcode-libarchive==3.5.1.210531
Faker==8.14.0
Flask==2.0.1
Flask-Babel==2.0.0
Flask-SQLAlchemy==2.5.1
Flask-WTF==0.15.1
gemfileparser==0.8.0
geoip2==4.4.0
gitdb==4.0.9
gitdb2==4.0.2
GitPython==3.0.6
greenlet==1.1.1
gunicorn==20.1.0
idna==3.2
idna-ssl==1.1.0
importlib-metadata==4.8.1
isodate==0.6.1
itsdangerous==2.0.1
javaproperties==0.8.1
Jinja2==3.0.1
jmespath==0.10.0
jsonstreams==0.6.0
license-expression==21.6.14
lxml==4.6.3
MarkupSafe==2.0.1
maxminddb==2.2.0
more-itertools==8.13.0
multidict==5.1.0
newrelic==7.0.0.166
normality==2.3.3
packageurl-python==0.9.9
packaging==21.3
parameter-expansion-patched==0.3.1
pip-requirements-parser==31.2.0
pkginfo==1.8.2
ply==3.11
publicsuffix2==2.20191221
pyahocorasick==1.4.4
pycparser==2.21
pygmars==0.7.0
Pygments==2.12.0
PyJWT==2.4.0
pyparsing==3.0.9
python-dateutil==2.8.2
python-docx==0.8.11
python-owasp-zap-v2.4==0.0.20
pytz==2021.1
PyYAML==5.4.1
requests==2.27.1
s3transfer==0.5.0
saneyaml==0.5.2
sentry-sdk==1.4.1
six==1.16.0
smmap==5.0.0
soupsieve==2.3.2.post1
SQLAlchemy==1.4.25
text-unidecode==1.3
toml==0.10.2
tornado==6.1
truffleHog==2.2.1
truffleHogRegexes==0.0.7
typecode-libmagic==5.39.210531
types-setuptools==57.4.14
types-toml==0.10.7
typing-extensions==3.10.0.2
urllib3==1.26.7
urlpy==0.5
wcwidth==0.2.5
webencodings==0.5.1
Werkzeug==2.0.1
ws-bulk-report-generator==0.3.0
ws-sdk==0.9.1
WTForms==2.2.1
XlsxWriter==3.0.3
xmltodict==0.13.0
yarl==1.6.3
zipp==3.5.0
zope.interface==5.4.0
