import argparse
import requests
import os

def upload_results(host_url, api_key, project_id, result_file, verify=True): # set verify to False if ssl cert is self-signed

    headers = dict()
    files = dict()

    # Prepare headers
    # headers = {'Authorization': 'Token 3e24a3ee5af0305af20a5e6224052de3ed2f6859'}
    # headers['Content-Type'] = 'multipart/form-data'
    headers['X-API-Key'] = api_key

    # Prepare file data to send to API
    files['project'] = project_id
    files["bom"] = open(result_file, 'rb')

    # Make a request to API
    response = requests.post(host_url, headers=headers, files=files, verify=verify)
    # print(response.request.body)
    # print(response.request.headers)
    print(response.status_code)
    # print(response.text)
    return response.status_code

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='CI/CD integration for DefectDojpendency Track')
    parser.add_argument('--host_url', help="Dependency Track Host URL ", required=True)
    parser.add_argument('--api_key', help="API Key", required=True)
    parser.add_argument('--project_id', help="Dependency Track Project ID", required=False)
    parser.add_argument('--result_dir', help="Parent directory of security scans", required=True)
    
    # Parse out arguments
    args = vars(parser.parse_args())
    host_url = args["host_url"]
    api_key = args["api_key"]
    project_id = args["project_id"]
    result_dir = args["result_dir"]
 
    print(f'host_url: {host_url}')
    print(f'api_key: {api_key}')
    print(f'project_id: {project_id}')
    print(f'result_dir: {result_dir}')

    for scope in ["app", "container"]:
        result_folder = os.path.join(result_dir,scope)
        for root, subdirs, files in os.walk(result_folder):
            if len(files) > 0:
                for file in files:
                    result_file = os.path.join(root,file)
                    print(f'Uploading sbom data from file {result_file}')
                    result = upload_results(host_url, api_key, project_id, result_file)

                    if (result == 200) or (result == 201):
                        print("Successfully uploaded the results to Dependency Track")
                    else:
                        print(f"Something went wrong, please debug. Error {result}")
